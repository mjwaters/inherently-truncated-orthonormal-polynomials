![image info](example.png)

# Inherently Truncated Orthonormal Polynomials
For MLIPs that don't need cutoff functions. 

Clone it. Add `itop` to your `PYTHONPATH`. 
Run the example to see how to get started. 


## License
MIT License

