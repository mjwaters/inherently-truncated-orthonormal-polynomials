
import numpy as np
from scipy.integrate import quad
import sympy as sp


def generate_polybasis_by_gram_schmidt(coeff_generator, nmax=6, mode='np', verbose=True):
    p_list=[]
    for i in range(nmax):
        coeff = coeff_generator(i,nmax)
        new_p = Polynomial(coeff, mode=mode)
        new_p.normalize()
        for j in range(i):
            old_p = p_list[j]
            # can't do this with lists
                #new_p.coeff =  new_p.coeff -  poly_proj(new_p, old_p)*old_p.coeff
            # therefore we have to loop
            proj = poly_proj(new_p, old_p)
            for coei in range(len(new_p.coeff)):
                new_p.coeff[coei] =  new_p.coeff[coei] -  proj*old_p.coeff[coei]
            ####
            new_p.normalize()
        if verbose:
            print('poly index',i ,'->',coeff, '\n ', new_p.coeff)
        p_list.append(new_p)
    return p_list

def list_zeros(n):
    return [0 for _ in range(n)]


class CoeffGenerator:
    # basically a function to spit out Polynomial coeff guesses like
    # [0. 0. 1. 1. 1. 0. 0. 0. 0.] when given maximum number of terms (for poly order)
    # and the requested term index
    def __init__(self, starting_order=2, alternate_sign=False):
        self.starting_order=starting_order
        self.alternate_sign=alternate_sign

    def __call__(self, n_term, n_max_terms):
        max_order = self.starting_order + n_max_terms
        #coeff = np.zeros(max_order)
        coeff = list_zeros(max_order)
        for order in range(self.starting_order, self.starting_order+1+n_term):
            if not self.alternate_sign:
                coeff[order]=1
            else:
                #print(order-self.starting_order)
                sgn = (-1)**(order-self.starting_order)
                coeff[order]=int(sgn)
        return coeff



def poly_proj(polya, polyb):
    if polya.mode=='sp':
        expr_a = polya.poly_expr_sp()
        expr_b = polyb.poly_expr_sp()
        proj = sp.integrate(expr_a*expr_b, (polya.x, 0, 1) )
    else:
        #def integrand(x):
        #    return polya(x)*polyb(x)
        result = quad(lambda x: polya(x)*polyb(x), 0, 1)
        proj = result[0]
    return proj

from copy import deepcopy

class Polynomial:
    def __init__(self, coeff, mode='np', poly_sym=None):
        self.mode=mode
        if poly_sym is None:
            self.x = sp.symbols('x')
        self.coeff=deepcopy(coeff)

    def __call__(self, x):
        coeff_floats = self.coeff_as_float()
        y = np.polyval( np.flip(coeff_floats), x)
        return y

    def coeff_as_float(self):
        return [float(coe) for coe in self.coeff ]

    def normalize(self):
        proj = poly_proj(self, self)
        if self.mode=='sp':
            norm_coeff = sp.sqrt(proj)
        else:
            norm_coeff = np.sqrt(proj)
        for i in range(len(self.coeff)):
            self.coeff[i] = self.coeff[i]/norm_coeff

    def poly_expr_sp(self):
        poly_expr = sp.Integer(0)
        for i in range(len(self.coeff)):
            poly_expr+=self.coeff[i] * (self.x**i)
        return poly_expr

    def poly_expr_sp_radial_form(self, radial_symbol=sp.symbols('r')):
        expr_r = self.poly_expr_sp().subs(self.x, 1-radial_symbol)
        return sp.expand(expr_r)

    def derivative_np(self,m=1):
        n = len(self.coeff)
        cder = np.zeros(n)
        pd = np.polyder( reversed(self.coeff), m=m)
        cder[0:n-m]= np.flip(pd)
        return Polynomial(cder, mode=self.mode)

    def derivative_sp(self,m=1):
        newpoly = diff(self.poly_expr_sp(), self.x, 3)
        # todo
        # need to copy the coeff to a new Polynomial object

    def derivative(self, m=1):
        if self.mode=='sp':
            return self.derivative_sp(m=m)
        else:
            return self.derivative_np(m=m)


def default_polynomial_basis(nmax, mode='np', starting_order=2):
    cg = CoeffGenerator(starting_order=starting_order)
    p_list = generate_polybasis_by_gram_schmidt(cg, nmax=nmax, mode=mode, verbose=False)
    return p_list



####### this should be depricated ###
def initial_coeff_guess(n, nmax, starting_order=2):
    coeff = np.zeros(nmax+starting_order)
    coeff[starting_order:(n+starting_order+1)]=1
    return coeff
