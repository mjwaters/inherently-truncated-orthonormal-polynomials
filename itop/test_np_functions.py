
import numpy as np
from core import Polynomial, poly_proj
from core import CoeffGenerator, generate_polybasis_by_gram_schmidt


if True:
    cg = CoeffGenerator(starting_order=2)
    print('------')
    n_terms=6
    for term_index in range(n_terms):
        print(cg(term_index, n_terms))
    print('------')



if False:
    x=np.linspace(0,1,6)
    print(x)
    #print( np.polyval( [1,0,0], x))

    p2 = Polynomial([0,0,1,0,0])
    print(p2(x))

    print(poly_proj(p2,p2))

    p2.normalize()
    print(p2.coeff)

    i = 0
    nmax = 5
    coeff=coeff_init_sequential(i+2,nmax)
    new_p = Polynomial(coeff)
    new_p.normalize()

    print(coeff)
    #p_list.append(new_p)
    p2_2 = p2.derivative_np(2)
    print('d2p/dx2',p2_2.coeff)

    #################

    p3 = Polynomial([0,0,1,1,0])
    p3.normalize()
    p3.coeff =  p3.coeff -  poly_proj(p3,p2)*p2.coeff
    p3.normalize()
    print(p3.coeff)


    ###################
    p4 = Polynomial([0,0,1,1,1])
    p4.normalize()
    p4.coeff =  p4.coeff -  poly_proj(p4,p2)*p2.coeff
    p4.normalize()

    p4.coeff =  p4.coeff -  poly_proj(p4,p3)*p3.coeff
    p4.normalize()
    print(p4.coeff)


###################
from matplotlib import pyplot as plt
if False:
    xs = np.linspace(0,1,50)

    fig, ax = plt.subplots()

    ax.plot(1-xs,p2(xs))
    ax.plot(1-xs,p3(xs))
    ax.plot(1-xs,p4(xs))
    print(poly_proj(p3,p4))

    plt.show()



##############
nmax = 7
xs = np.linspace(0,1,200)
###########


print('\ncoeff_init starting_order=2')
if True:
    cg = CoeffGenerator(starting_order=2)
    p_list = generate_polybasis_by_gram_schmidt(cg, nmax=nmax)

    if True:
        fig, ax = plt.subplots()
        ax.set_title('coeff_init starting_order=2')
        ax.axhline(0, color='k')
        for i, poly in enumerate(p_list):
            ax.plot(1-xs,poly(xs), label=str(i))
        ax.set_xlim(0,1)
        ax.legend()


##############

print('\ncoeff_init starting_order=2 alternate_sign')
if True:
    cg = CoeffGenerator(starting_order=2, alternate_sign=True)
    p_list = generate_polybasis_by_gram_schmidt(cg, nmax=nmax)

    if True:
        fig, ax = plt.subplots()
        ax.set_title('coeff_init starting_order=2 alternate_sign')
        ax.axhline(0, color='k')
        for i, poly in enumerate(p_list):
            ax.plot(1-xs,poly(xs), label=str(i))
        ax.set_xlim(0,1)
        ax.legend()




################
print('\ncoeff_init starting_order=3')

if True:
    cg = CoeffGenerator(starting_order=3)
    p_list = generate_polybasis_by_gram_schmidt(cg, nmax=nmax)

    if True:
        fig, ax = plt.subplots()
        ax.set_title('coeff_init_seq3')
        ax.axhline(0, color='k')
        for i, poly in enumerate(p_list):
            ax.plot(1-xs,poly(xs), label=str(i))
        ax.set_xlim(0,1)
        ax.legend()
##################




print('\ncoeff_init starting_order=1')

if True:
    cg = CoeffGenerator(starting_order=1)
    p_list = generate_polybasis_by_gram_schmidt(cg, nmax=nmax)

    if True:
        fig, ax = plt.subplots()
        ax.set_title('coeff_init_seq1')
        ax.axhline(0, color='k')
        for i, poly in enumerate(p_list):
            ax.plot(1-xs,poly(xs), label=str(i))
        ax.set_xlim(0,1)
        ax.legend()


################ Coeff overrun is somewhat unreliable trick, don't use it.
if False: ## over runs are interesting for higher starting orders,
    print('\n overrun polynomials')
    nmax= 8
    starting_order = 3
    overrun_size = 1 # less than starting order
    from core import initial_coeff_guess

    p_list=[]
    for i in range(nmax+overrun_size):
        coeff = initial_coeff_guess(i,nmax, starting_order=starting_order)
        new_p = Polynomial(coeff)
        new_p.normalize()
        for j in range(i):
            old_p = p_list[j]
            new_p.coeff =  new_p.coeff -  poly_proj(new_p, old_p)*old_p.coeff
            new_p.normalize()
        print(i,'->',coeff, '\n ', list(np.round(new_p.coeff, 8) ))

        p_list.append(new_p)
        if len(p_list)>=2:
            print('    proj([-1],[-2]):', np.round( poly_proj(p_list[-1], p_list[-2]), 8 ))

    if True:
        fig, ax = plt.subplots()
        ax.set_title('coeff_overrun')
        ax.axhline(0, color='k')
        for i, poly in enumerate(p_list):
            ax.plot(1-xs,poly(xs), label=str(i))
        ax.set_xlim(0,1)
        ax.legend()


##############
plt.show()
