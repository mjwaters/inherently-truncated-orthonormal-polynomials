

import numpy as np
import sympy as sp
from core import Polynomial, poly_proj
from core import CoeffGenerator, generate_polybasis_by_gram_schmidt


if False:
    p2 = Polynomial([0,0,1], mode='sp')
    print(p2.poly_expr_sp())
    ##
    proj=poly_proj(p2,p2)
    print(proj)
    ##
    for i in range(len(p2.coeff)):
        p2.coeff[i]=p2.coeff[i]/sp.sqrt(proj)
    print(p2.poly_expr_sp())


#################
nmax=5

from matplotlib import pyplot as plt
xs = np.linspace(0,1,200)



if True:
    print('\ncoeff_init starting_order=2')
    cg = CoeffGenerator(starting_order=2)
    p_list = generate_polybasis_by_gram_schmidt(cg, nmax=nmax, mode='sp')
    for i, poly in enumerate(p_list):
        print('\nradial form polynomial index %i:'%i)
        print(poly.poly_expr_sp_radial_form())

    if True:
        fig, ax = plt.subplots()
        ax.set_title('coeff_init starting_order=2')
        ax.axhline(0, color='k')
        for i, poly in enumerate(p_list):
            ax.plot(1-xs,poly(xs), label=str(i))
        ax.set_xlim(0,1)
        ax.legend()

plt.show()
