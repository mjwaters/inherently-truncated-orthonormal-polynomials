
from itop import default_polynomial_basis
poly_list = default_polynomial_basis(
    nmax=5,
    mode = 'np') # mode can also be 'sp' for sympy


# print the coefficients
for index, poly in enumerate(poly_list):
    print(index, poly.coeff)

#poly is a function of (x)
#where  radius = 1-x

# plot the polynomials in radius:
import numpy as np
from matplotlib import pyplot as plt
xs = np.linspace(0,1,200)

fig, ax = plt.subplots()
ax.axhline(0, color='k')
for i, poly in enumerate(poly_list):
    ax.plot(1-xs,poly(xs), label=str(i))
ax.set_xlim(0,1)
ax.set_xlabel('r/rcut')
ax.legend()

plt.show()
